#!/usr/bin/python3

import base64, sys, os, flask, json

application = flask.Flask(__name__)
db = json.loads(open("/var/www/ftn-ebook-tmp/db.json").read())

@application.route("/get/<epubID>")
def get(epubID):
    san = ""
    for c in epubID:
        if ord(c) >= 48 and ord(c) <= 57:
            san += c
    return base64.b64encode(open("/var/www/ftn-ebook-tmp/zips/" + san + ".zip", 'rb').read())

@application.route("/search/<querys>")
def search(querys):
    global db

    san = ""

    for c in querys:
        if (ord(c) >= 65 and ord(c) <= 90) or (ord(c) >= 97 and ord(c) <= 122) or c == '+':
           san  += c

    query = san.replace('++', ',').replace('+', ' ').split(',')
    db_new = {}

    for epubID in db:
        if not "genres" in db[epubID]:
            continue
        
        temp = query.copy()

        for genre in db[epubID]["genres"]:
            if genre in temp:
                temp.remove(genre)
        if len(temp) == 0:
            db_new[epubID] = db[epubID]

    return json.dumps(db_new)

@application.route("/")
def main():
    global db

    r = []
    for epubID in db:
        r.append(["0" + epubID, db[epubID]["title"]])
    return json.dumps(r)

if __name__ == "__main__":
    application.run(host="0.0.0.0", port=80, debug=False)
