#!/bin/sh

for epub in $(ls *.epub); do
	mkdir $(echo $epub | cut -d'.' -f1)
	cp $epub $(echo $epub | cut -d'.' -f1)
	cd $(echo $epub | cut -d'.' -f1)
	unzip $epub
	mv cover_image.jpg ../$(echo $epub | cut -d'.' -f1).jpg
	cd ..
	rm -r $epub $(echo $epub | cut -d'.' -f1)
done
