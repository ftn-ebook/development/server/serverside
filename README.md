### Searching
```sh
# list all books
curl http://url/

# search using tags for specific book
curl http://url/search/tag+1++tag+2++tag+3
```

### downloading
```sh
# download book by ID
curl http://url/get/epub-id
```
