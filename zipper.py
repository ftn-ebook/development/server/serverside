#!/usr/bin/python3

import json, os

db = json.loads(open("db.json", "r").read())

for epubID in db:
    open("data.json", "w").write(json.dumps(db[epubID]))
    os.system("cp images/0" + epubID + ".jpg ./cover.jpg")
    os.system("cp epubs/0" + epubID + ".epub ./book.epub")
    os.system("zip 0" + epubID + ".zip cover.jpg book.epub data.json")
    os.system("mv 0" + epubID + ".zip zips")
    os.system("rm 0" + epubID + ".zip cover.jpg book.epub data.json")
